public interface SqlCrud {

    static final String sqlQueryInsertClients = "INSERT clients(id, name, age, idaccount) VALUES(1, 'Iryna', 39, 1), (2, 'Valerii', 31, 2), (3, 'Max', 35, 3), (4, 'Ylia', 28, 4), (5, 'Olga', 22, 5);";
    static final String sqlQueryInsertAccounts = "INSERT INTO accounts(id, account, balance) VALUES(1, 12001, 12000), (2, 12002, 15000), (3, 12003, 13000), (4, 12004, 10000), (5, 12005, 25000);";
    static final String sqlQuerySelect = "SELECT * FROM clients AS cl JOIN accounts AS ac ON cl.idAccount = ac.id";
    static final String sqlQueryUpdateClients = "UPDATE clients SET age = 25 WHERE id = 5;";
    static final String sqlQueryUpdateAccounts = "UPDATE accounts SET balance = 20000 WHERE id = 1;";
    static final String sqlQueryDeleteClients = "DELETE FROM clients WHERE id IN (4,5);";
    static final String sqlQueryDeleteAccounts = "DELETE FROM accounts WHERE id IN (4,5);";

    void sqlQuery();
}
