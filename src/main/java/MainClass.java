public class MainClass{

    public static void main(String[] args) {

        new SqlInsert().sqlQuery();
        System.out.println("Intermediate results:");
        new SqlSelect().sqlQuery();
        new SqlUpdate().sqlQuery();
        new SqlDelete().sqlQuery();
        System.out.println("Final results:");
        new SqlSelect().sqlQuery();
    }
}
