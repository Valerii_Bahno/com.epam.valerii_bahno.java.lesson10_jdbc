import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

public class SqlDelete implements SqlCrud {

    private static final Logger logger = LogManager.getLogger(SqlUpdate.class);

    @Override
    public void sqlQuery() {

        try (Connection connection = DriverManager
                .getConnection("jdbc:mysql://localhost:3306/test", "root", "")) {

            try (Statement statement = connection.createStatement()) {
                ResultSet resultSetClients = statement.executeQuery(sqlQueryDeleteClients);
                ResultSet resultSetAccounts = statement.executeQuery(sqlQueryDeleteAccounts);
                logger.info("Delete successfully completed");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
