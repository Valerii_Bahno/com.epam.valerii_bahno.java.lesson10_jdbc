import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

public class SqlSelect implements SqlCrud {

    private static final Logger logger = LogManager.getLogger(SqlSelect.class);

    @Override
    public void sqlQuery() {

        try (Connection connection = DriverManager
                .getConnection("jdbc:mysql://localhost:3306/test", "root", "")) {

            try (Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery(sqlQuerySelect);
                logger.info("Select successfully completed");

                ResultSetMetaData rsmd = resultSet.getMetaData();
                while (resultSet.next()) {
                    for (int i = 1; i <= 7; i++) {
                        if (i > 1) System.out.print(",  ");
                        String columnValue = resultSet.getString(i);
                        System.out.print(columnValue + " " + rsmd.getColumnName(i));
                    }
                    System.out.println("");
                }
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
