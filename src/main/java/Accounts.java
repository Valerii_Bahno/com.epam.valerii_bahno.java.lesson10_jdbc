import java.io.Serializable;

public class Accounts implements Serializable {

    private int id;
    private int account;
    private double balance;

    public Accounts(int id, int account, double balance) {
        this.id = id;
        this.account = account;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public int getAccount() {
        return account;
    }

    public double getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return "Accounts{" +
                "id=" + id +
                ", account=" + account +
                ", balance=" + balance +
                '}';
    }
}
