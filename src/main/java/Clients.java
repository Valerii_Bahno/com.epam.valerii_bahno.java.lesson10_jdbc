import java.io.Serializable;

public class Clients implements Serializable {

    private int id;
    private String name;
    private String surname;
    private int age;
    private int idAccount;

    public Clients(int id, String name, String surname, int age, int idAccount) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.idAccount = idAccount;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public int getIdAccount() {
        return idAccount;
    }

    @Override
    public String toString() {
        return "Clients{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", idAccount=" + idAccount +
                '}';
    }
}
