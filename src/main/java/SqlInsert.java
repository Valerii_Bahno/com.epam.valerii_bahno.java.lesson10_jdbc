import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

public class SqlInsert implements SqlCrud {

    private static final Logger logger = LogManager.getLogger(SqlInsert.class);

    @Override
    public void sqlQuery() {

        try (Connection connection = DriverManager
                .getConnection("jdbc:mysql://localhost:3306/test", "root", "")) {
            try (Statement statement = connection.createStatement()) {
                ResultSet resultSetClient = statement.executeQuery(sqlQueryInsertClients);
                ResultSet resultSetAccount = statement.executeQuery(sqlQueryInsertAccounts);
                logger.info("Insert successfully completed");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
